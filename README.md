# README #

This project holds the configuration settings for the IEW JMS Bus and some client connection examples

### Java Client Examples ###

* JmsTopicPub - for collocated applications to publish via Jms
* JmsTopicSub - for collocated applications to subscribe via Jms
* RestTopicPub - for external applications to publish via Rest
* RestTopicSub - for external applications to subscribe via Rest
* Example messages - JmsBus / java-client-examples / src / main / resources /

### Running JMS Test Web App ###

* cd java-client-examples
* mvn clean jetty:run
* http://localhost:9999/org.cbrnwatchboard.jms.examples/

### Running server with Docker ###

* docker build -t iew-bus .
* docker run -i -p 8161:8161 iew-bus