package org.cbrnwatchboard.jms.rest;

import java.io.*;
import javax.jms.JMSException;
import javax.servlet.http.HttpServletResponse;

import org.cbrnwatchboard.jms.examples.JmsTopicPub;
import org.slf4j.*;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * Created by gdonarum on 6/2/2016.
 */
@RequestMapping("/examples")
public class ExamplesController {
    private static final Logger logger = LoggerFactory.getLogger(ExamplesController.class);
    private JmsTopicPub sender = null;

    /**
     * Get a list of topics.  This is for the dropdown list.
     * @param response
     */
    @RequestMapping(method=RequestMethod.GET, value="/topics", produces = "application/json")
    public void getTopics(HttpServletResponse response) {
        logger.debug("GET /examples/topics");
        // respond
        try {
            response.getWriter().write(readFileAsString("topics.json"));
        } catch (IOException e) {
            logger.error(e.getMessage());
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Get an example message for the selected topic.
     * @param name
     * @param response
     */
    @RequestMapping(method=RequestMethod.GET, value="/topic/{name:.+}", produces = "text/plain")
    public void getTopicExampleByName(@PathVariable String name, HttpServletResponse response) {
        logger.debug("GET /examples/topic/" + name);
        // respond
        try {
            response.getWriter().write(readFileAsString(name));
        } catch (IOException e) {
            logger.error(e.getMessage());
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Post a message to a JMS topic.
     * @param messageString
     * @param topic
     * @param response
     */
    // Create = POST to a base URI returning a newly created URI
    // {Update = POST can also map to Update although it's typically used for Create. POST can also be a partial update so we don't need the proposed PATCH method.}
    @RequestMapping(method=RequestMethod.POST, value="/message/{topic:.+}" ,consumes ="application/json", produces = "application/json")
    public void create(@RequestBody String messageString, @PathVariable String topic, HttpServletResponse response) {
        logger.debug("POST/CREATE - /examples/message/" + topic);
        String out = "";
        try {
            logger.debug("POST/CREATE " + messageString);
            getPublisher().sendMessage(topic,messageString);
            return;
        } catch (Exception e1) {
            e1.printStackTrace();
            out = e1.getMessage();
            logger.error(e1.getMessage());
        }
        // respond
        try {
            response.getWriter().write(out);
        } catch (IOException e) {
            logger.error(e.getMessage());
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    private JmsTopicPub getPublisher() throws JMSException {
        if(sender == null) {
            sender = new JmsTopicPub();
        }
        return sender;
    }

    public String readFileAsString(String filePath) {
        try {
            StringBuffer fileData = new StringBuffer(1000);
            BufferedReader reader = new BufferedReader(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream(filePath)));
            char[] buf = new char[1024];
            int numRead = 0;
            while ((numRead = reader.read(buf)) != -1) {
                String readData = String.valueOf(buf, 0, numRead);
                fileData.append(readData);
                buf = new char[1024];
            }
            reader.close();
            return fileData.toString();
        } catch(Exception e) {
            e.printStackTrace();
            return "ERROR: " + filePath + " - " + e.getMessage();
        }
    }
}
