package org.cbrnwatchboard.jms.sockjs;

/**
 * Created by gdonarum on 6/7/2016.
 */
public class HelloMessage {

    private String name;

    public HelloMessage() {}

    public HelloMessage(String name) {
        this.name=name;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name=name;
    }
}
