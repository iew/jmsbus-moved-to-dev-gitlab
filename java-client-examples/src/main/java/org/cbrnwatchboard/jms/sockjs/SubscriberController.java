package org.cbrnwatchboard.jms.sockjs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import javax.jms.*;
import javax.jms.Message;
import java.text.SimpleDateFormat;

/**
 * Created by gdonarum on 6/7/2016.
 */
@Controller
public class SubscriberController implements MessageListener {


    private SimpMessagingTemplate template;

    @Autowired
    public SubscriberController(SimpMessagingTemplate template) {
        this.template = template;
    }

//    From tutorial: https://spring.io/guides/gs/messaging-stomp-websocket/
//    @MessageMapping("/hello")
//    @SendTo("/topic/greetings")
//    public FeedMessage greeting(HelloMessage message) throws Exception {
//        System.out.println("RCVD: " + message.getName());
//        //Thread.sleep(3000); // simulated delay
//        return new FeedMessage("connect","Hello, " + message.getName() + "!");
//    }

    public void onMessage(Message message) {
        System.out.println("MSG HEARD: " + message);
        TextMessage textMessage = (TextMessage) message;
        FeedMessage msg = new FeedMessage();
        msg.setTopic("topic");
        try {
            msg.setBody(textMessage.getText());
        } catch (Exception e) {
            e.printStackTrace();
            msg.setBody(e.getMessage());
        }
        try {
            java.util.Date date = new java.util.Date(message.getJMSTimestamp());
            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            this.template.convertAndSend("/topic/feed",message.getJMSDestination() + " [" + sdf.format(date) + "]  ==>  " + ((TextMessage) message).getText());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
