package org.cbrnwatchboard.jms.sockjs;

/**
 * Created by gdonarum on 6/7/2016.
 */
public class FeedMessage {
    private String topic;
    private String body;

    public FeedMessage() {
        this("unset","unset");
    }

    public FeedMessage(String topic, String body) {
        this.topic=topic;
        this.body=body;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
