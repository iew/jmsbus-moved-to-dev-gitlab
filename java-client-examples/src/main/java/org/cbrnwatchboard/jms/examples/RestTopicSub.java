package org.cbrnwatchboard.jms.examples;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.DatatypeConverter;

/**
 * Created by gdonarum on 5/4/2016.
 */
public class RestTopicSub {
    long clientId = (new Date()).getTime();
    public void sendGet() {

        try {
            //String url = "http://localhost:8161/api/message/test";
            String url = "http://iew.cbrnwatchboard.org/activemq-api/message/env.sensor.alarm.create";
            
            //need to add clientId so it remebers who the consumer is between requests
            url = url + "?clientId=" + clientId;
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            String auth = "admin:admin";
            String encoding = DatatypeConverter.printBase64Binary(auth.getBytes("UTF-8"));
            
            //add reuqest header
            con.setRequestMethod("GET");
            con.setRequestProperty("Authorization", "Basic " + encoding);
            
            int status = con.getResponseCode();
            BufferedReader in;
            if(status >= 200 && status < 400){
                in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            }
            else{
                 in =  new BufferedReader(
                    new InputStreamReader(con.getErrorStream()));
            }
            String inputLine;
            StringBuffer response = new StringBuffer();
            
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            String message = response.toString();
            if(message.length() > 0){
                //print result
                System.out.println("rest consumer: " + response.toString());
                sendGet();
            }
        } catch (IOException ex) {
            Logger.getLogger(RestTopicPub.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    public static void main(String[] args) throws InterruptedException{
        RestTopicSub rest = new RestTopicSub();
        while(true){
            rest.sendGet();
            Thread.sleep(10000);
        }
    }
}
