package org.cbrnwatchboard.jms.examples;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.DatatypeConverter;



/**
 * Created by gdonarum on 5/4/2016.
 */
public class RestTopicPub {
    public void sendPost(String payload) {

        try {
            //String url = "http://iew.cbrnwatchboard.org/activemq-api/message?destination=topic://env.sensor.alarm.create";
            String url = "http://localhost:8161/api/message?destination=topic://map.feature.plot.url";
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            String auth = "admin:admin";
            String encoding = DatatypeConverter.printBase64Binary(auth.getBytes("UTF-8"));

            //add reuqest header
            con.setRequestMethod("POST");
            con.setRequestProperty("Authorization", "Basic " + encoding);
            // Send post request
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes("body=" + payload);
            wr.flush();
            wr.close();

            int status = con.getResponseCode();
            BufferedReader in;
            if(status >= 200 && status < 400){
                in = new BufferedReader(
                        new InputStreamReader(con.getInputStream()));
            }
            else{
                in =  new BufferedReader(
                        new InputStreamReader(con.getErrorStream()));
            }
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            //print result
            System.out.println(response.toString());
        } catch (IOException ex) {
            Logger.getLogger(RestTopicPub.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    public static void main(String[] args) throws Exception{
        RestTopicPub rest = new RestTopicPub();
        rest.sendPost("{" +
                "\"sampleId\": \"esa1\"," +
                "\"sensorType\": \"BioSensor\"," +
                "\"sensorId\": \"sensor1\"," +
                "\"sampleTime\": \"2016-06-02T18:25:43.511Z\"," +
                "\"locationLat\": \"39.466032\"," +
                "\"locationLon\": \"-76.305535\"," +
                "\"agent\": \"Bacillus anthracis\"," +
                "\"ctValue\": \"32.5\"," +
                "\"result\": \"PRESUMPTIVE_POSITIVE\"" +
                "}");
//        rest.sendPost("{\n" +
//                "\"overlayId\":\"JMS\",\n" +
//                "\"featureId\":\"1234\",\n" +
//                "\"url\":\"http://dhs.tacbrd.org/tacdogsKML/\",\n" +
//                "\"name\":\"CMAPI\",\n" +
//                "\"format\":\"kml\",\n" +
//                "\"zoom\":\"true\"\n" +
//                "}");
    }
}