/*
 * Test client to listen for new topic messages
 */
package org.cbrnwatchboard.jms.examples;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

public class JmsMessageListener implements MessageListener {
	private String consumerName;

	public JmsMessageListener() {
		this.consumerName = "Default Consumer";
	}
        
	public JmsMessageListener(String consumerName) {
		this.consumerName = consumerName;
	}

	public void onMessage(Message message) {
		TextMessage textMessage = (TextMessage) message;
		try {
			System.out.println(consumerName + " received: "
					+ textMessage.getText());
			System.out.println("\t\tComplete Msg:  "
					+ textMessage);
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

}

