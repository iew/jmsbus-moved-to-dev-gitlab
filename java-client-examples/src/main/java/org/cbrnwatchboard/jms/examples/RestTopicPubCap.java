package org.cbrnwatchboard.jms.examples;

import javax.xml.bind.DatatypeConverter;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Created by gdonarum on 5/4/2016.
 */
public class RestTopicPubCap {
    public void sendPost(String payload) {

        try {
            String url = "https://iew.cbrnwatchboard.org/activemq-api/message?destination=topic://intel.cap";
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            String auth = "admin:admin";
            String encoding = DatatypeConverter.printBase64Binary(auth.getBytes("UTF-8"));

            //add reuqest header
            con.setRequestMethod("POST");
            con.setRequestProperty("Authorization", "Basic " + encoding);
            // Send post request
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes("body=" + payload);
            wr.flush();
            wr.close();

            int status = con.getResponseCode();
            BufferedReader in;
            if(status >= 200 && status < 400){
                in = new BufferedReader(
                        new InputStreamReader(con.getInputStream()));
            }
            else{
                in =  new BufferedReader(
                        new InputStreamReader(con.getErrorStream()));
            }
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            //print result
            System.out.println(response.toString());
        } catch (IOException ex) {
            Logger.getLogger(RestTopicPubCap.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    public static void main(String[] args) throws Exception{
        RestTopicPubCap rest = new RestTopicPubCap();
        rest.sendPost("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
                "<alert xmlns=\"urn:oasis:names:tc:emergency:cap:1.2\">\n" +
                "    <identifier>SR:_291742ZMAY2012</identifier>\n" +
                "    <sender>OMI001</sender>\n" +
                "    <sent>2013-07-03T10:52:25-04:00</sent>\n" +
                "    <status>Exercise</status>\n" +
                "    <msgType>Alert</msgType>\n" +
                "    <scope>Public</scope>\n" +
                "    <note>Exercise Nickname: TEST MESSAGES</note>\n" +
                "    <info>\n" +
                "        <language>en-US</language>\n" +
                "        <category>CBRNE</category>\n" +
                "        <event>CBRN Incident</event>\n" +
                "        <urgency>Unknown</urgency>\n" +
                "        <severity>Unknown</severity>\n" +
                "        <certainty>Observed</certainty>\n" +
                "        <headline>Situation Report for CBRN Incident (SITREP)</headline>\n" +
                "        <description>This is where the GENTEXT content goes</description>\n" +
                "    </info>\n" +
                "</alert>\n");
    }
}