package org.cbrnwatchboard.jms.examples;

import java.net.URISyntaxException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class JmsTopicSub{

    public JmsTopicSub() {

    }
    //Run me to wait for messages
    public static void main(String[] args) throws URISyntaxException, Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        System.out.println("Listening for messages...");
    }
}

