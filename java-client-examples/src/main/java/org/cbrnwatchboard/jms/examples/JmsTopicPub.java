package org.cbrnwatchboard.jms.examples;

/**
 * Demonstration class for publishing to topics through the JMS port.
 */

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

public class JmsTopicPub {
    private ConnectionFactory factory = null;
    private Connection connection = null;
    private Session session = null;
    private Destination destination = null;
    private MessageProducer producer = null;

    public JmsTopicPub() throws JMSException {
        this(ActiveMQConnection.DEFAULT_BROKER_URL);
    }

    public JmsTopicPub(String brokerUrl) throws JMSException {
        System.out.println("Broker: " + brokerUrl);
        factory = new ActiveMQConnectionFactory(brokerUrl);
        connection = factory.createConnection();
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        destination = session.createTopic("map.feature.plot");
        producer = session.createProducer(destination);
    }

    public void shutdown() throws JMSException {
        connection.close();
    }

    public void sendMessage(String topic, String msg) throws JMSException {
        Destination destination = session.createTopic(topic);
        MessageProducer producer = session.createProducer(destination);
        TextMessage message = session.createTextMessage();
        message.setText(msg);
        producer.send(message);
        System.out.println("Sent: " + message.getText());
    }

    public void sendMessage(String msg) throws JMSException {
        TextMessage message = session.createTextMessage();
        message.setText(msg);
        producer.send(message);
        System.out.println("Sent: " + message.getText());
    }

    public static void main(String[] args) throws JMSException {
        // set up
        //JmsTopicPub sender = new JmsTopicPub();
        JmsTopicPub sender = new JmsTopicPub("tcp://localhost:61616");

        // loop through and send a set of messages
        for(int i=1; i<20; i++) {
            sender.sendMessage("Hello ...This is sample message " + i);
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        // clean up
        sender.shutdown();
    }
}
