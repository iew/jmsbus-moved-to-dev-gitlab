<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script>
    $.getJSON('rest/examples/topics', function(data){
        $.each(data, function(index,item) {
        console.log(item.name);
           $("#select-topic").append("<option value=" + item.name + ">" + item.name + "</option>");
        });
    });
    function getTopic(topic) {
        console.log("Selected: " + topic.value);
        $.get('rest/examples/topic/'+topic.value, function(data){
            $("#textarea-example").val(data);
        });
    }
    function postMessage() {
    console.log("Topic: " + $("#select-topic").val());
    console.log("Body: " + $("#textarea-example").val());
        $.ajax({
          type: "POST",
          url: 'rest/examples/message/'+$("#select-topic").val(),
          data: $("#textarea-example").val(),
          contentType: "application/text"
        });
    }
  </script>
    <script src="sockjs-0.3.4.js"></script>
    <script src="stomp.js"></script>
    <script>var ctx = "${pageContext.request.contextPath}"</script>
    <script type="text/javascript">
        var stompClient = null;

        console.log("Context root: " + ctx);

        function setConnected(connected) {
            document.getElementById('response').innerHTML = '';
            showGreeting("Connected.  Listening...");
        }

        function connect() {
            showGreeting("Connecting...");
            var socket = new SockJS(ctx+'/sockjs/hello');
            stompClient = Stomp.over(socket);
            stompClient.connect({}, function(frame) {
                setConnected(true);
                console.log('Connected: ' + frame);
                stompClient.subscribe('/topic/greetings', function(greeting){
                    showGreeting(JSON.parse(greeting.body).body);
                });
                stompClient.subscribe('/topic/feed', function(greeting){
                    showGreeting(greeting.body);
                });
            });
        }

        function disconnect() {
            if (stompClient != null) {
                stompClient.disconnect();
            }
            setConnected(false);
            console.log("Disconnected");
        }

        function sendName() {
            var name = document.getElementById('name').value;
            stompClient.send("/app/hello", {}, JSON.stringify({ 'name': name }));
        }

        function showGreeting(message) {
            var response = document.getElementById('response');
            var p = document.createElement('p');
            p.style.wordWrap = 'break-word';
            p.appendChild(document.createTextNode(message));
            response.insertBefore(p, response.firstChild);
        }
    </script>
</head>
<body onload="connect()">
<p>
<select name="topic" id="select-topic" onchange="getTopic(this);">
    <option value="0">Select a topic</option>
</select>
</p>
<p>
<textarea id="textarea-example" rows="15" cols="100">
</textarea>
</p>
<p>
    <input type="submit" value="Publish to Bus" onclick="postMessage();"/>
</p>
<p></p>
<div id="" style="overflow-y: scroll; height:400px; border-style: double;">
    <p id="response"></p>
</div>
</body>
</html>