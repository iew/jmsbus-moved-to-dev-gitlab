package org.cbrnwatchboard.jms.cmapi;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * map.feature.update
 * http://cmapi.org/versions/v1.3.0/docs/index.html#map.feature.update
 *
 * Update an existing feature on the map.
 *
 * Payload:
 *  {
 *      overlayId: string (optional),
 *      featureId: string (required),
 *      name: string (optional),
 *      newOverlayId: string (optional),
 *  }
 */
public class MapFeatureUpdate {

    private String overlayId;
    private String featureId;
    private String name;

    private String newOverlayId;

    public String getOverlayId() {
        return overlayId;
    }

    public void setOverlayId(String overlayId) {
        this.overlayId = overlayId;
    }

    public String getFeatureId() {
        return featureId;
    }

    public void setFeatureId(String featureId) {
        this.featureId = featureId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNewOverlayId() {
        return newOverlayId;
    }

    public void setNewOverlayId(String newOverlayId) {
        this.newOverlayId = newOverlayId;
    }
}
