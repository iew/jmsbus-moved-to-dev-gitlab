package org.cbrnwatchboard.jms.cmapi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.annotate.JsonRawValue;

/**
 * map.feature.plot
 * http://cmapi.org/versions/v1.3.0/docs/index.html#map.feature.plot
 *
 * Plots feature data on the map.
 *
 * Payload:
 *  {
 *      overlayId: string (optional),
 *      featureId: string (required),
 *      name: string (optional),
 *      format: string ["kml", "geojson"] default="kml" (optional),
 *      feature: object | string (required),
 *      zoom: boolean default=false (optional),
 *      readOnly: boolean default=true (optional),
 *      properties: object (optional)
 *  }
 */
public class MapFeaturePlot {

    public enum FORMAT {kml, geojson};

    private String overlayId;
    private String featureId = UUID.randomUUID().toString();
    private String name;
    private FORMAT format = FORMAT.kml;
    private Object feature;
    private boolean zoom = false;
    private boolean readOnly = true;
    private HashMap<String,String> properties = new HashMap<String,String>();

    public String getOverlayId() {
        return overlayId;
    }

    public void setOverlayId(String overlayId) {
        this.overlayId = overlayId;
    }

    public String getFeatureId() {
        return featureId;
    }

    public void setFeatureId(String featureId) {
        this.featureId = featureId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FORMAT getFormat() {
        return format;
    }

    public void setFormat(FORMAT format) {
        this.format = format;
    }
    @JsonRawValue
    public String getFeature() {
        return feature.toString();
    }
    public void setFeature(JsonNode feature) {
        this.feature = feature;
    }

    public boolean isZoom() {
        return zoom;
    }

    public void setZoom(boolean zoom) {
        this.zoom = zoom;
    }

    public boolean isReadOnly() {
        return readOnly;
    }

    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }

    public HashMap<String,String> getProperties() {
        return properties;
    }

    public void setProperties(HashMap<String,String> properties) {
        this.properties = properties;
    }
}
