package org.cbrnwatchboard.jms.cmapi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * map.feature.plot.url
 * http://cmapi.org/versions/v1.3.0/docs/index.html#map.feature.plot.url
 *
 * Have the map plot feature data from a Uniform Resource Locator (URL).
 *
 * Payload:
 *  {
 *      featureId: string (required),
 *      url: string (required),
 *      overlayId: string (optional),
 *      name: string (optional),
 *      format: string ["kml", "geojson"] default="kml" (optional),
 *      params: object (optional),
 *      zoom: boolean default=false (optional)
 *  }
 */
public class MapFeaturePlotUrl {

    public enum FORMAT {kml, geojson, wms};

    private String featureId = UUID.randomUUID().toString();
    private String url;
    private String overlayId;
    private String name;
    private FORMAT format = FORMAT.kml;
    private HashMap<String,String> params = new HashMap<String,String>();
    private boolean zoom = false;

    public String getFeatureId() {
        return featureId;
    }

    public void setFeatureId(String featureId) {
        this.featureId = featureId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getOverlayId() {
        return overlayId;
    }

    public void setOverlayId(String overlayId) {
        this.overlayId = overlayId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FORMAT getFormat() {
        return format;
    }

    public void setFormat(FORMAT format) {
        this.format = format;
    }

    public HashMap<String,String> getParams() {
        return params;
    }

    public void setParams(HashMap<String,String> params) {
        this.params = params;
    }

    public boolean isZoom() {
        return zoom;
    }

    public void setZoom(boolean zoom) {
        this.zoom = zoom;
    }
}
