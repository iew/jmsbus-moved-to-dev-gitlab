package org.cbrnwatchboard.jms.cmapi;

/**
 * map.overlay.remove
 * http://cmapi.org/versions/v1.3.0/docs/index.html#map.overlay.remove
 *
 * Remove entire overlay from the map.
 *
 * Payload:
 *  {
 *      overlayId: string (optional)
 *  }
 */
public class MapOverlayRemove {

    private String overlayId;

    public String getOverlayId() {
        return overlayId;
    }

    public void setOverlayId(String overlayId) {
        this.overlayId = overlayId;
    }
}
