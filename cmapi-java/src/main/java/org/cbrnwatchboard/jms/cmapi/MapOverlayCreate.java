package org.cbrnwatchboard.jms.cmapi;

import java.util.ArrayList;
import java.util.List;

/**
 * map.overlay.create
 * http://cmapi.org/versions/v1.3.0/docs/index.html#map.overlay.create
 *
 * Create an overlay into which data can be aggregated.
 *
 * Payload:
 *  {
 *      name: string (optional),
 *      overlayId: string (optional),
 *      parentId: string (optional),
 *      properties: object (optional)
 *  }
 */
public class MapOverlayCreate {

    private String name;
    private String overlayId;
    private String parentId;
    private List<String> properties = new ArrayList<String>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOverlayId() {
        return overlayId;
    }

    public void setOverlayId(String overlayId) {
        this.overlayId = overlayId;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public List<String> getProperties() {
        return properties;
    }

    public void setProperties(List<String> properties) {
        this.properties = properties;
    }
}
