package org.cbrnwatchboard.jms.cmapi;

/**
 * map.feature.unplot
 * http://cmapi.org/versions/v1.3.0/docs/index.html#map.feature.unplot
 *
 * Removes feature data from the map.
 *
 * Payload:
 *  {
 *      overlayId: string (optional),
 *      featureId: string (required)
 *  }
 */
public class MapFeatureUnplot {

    private String overlayId;
    private String featureId;

    public String getOverlayId() {
        return overlayId;
    }

    public void setOverlayId(String overlayId) {
        this.overlayId = overlayId;
    }

    public String getFeatureId() {
        return featureId;
    }

    public void setFeatureId(String featureId) {
        this.featureId = featureId;
    }
}
