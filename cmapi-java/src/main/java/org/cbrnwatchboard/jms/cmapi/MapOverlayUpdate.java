package org.cbrnwatchboard.jms.cmapi;

/**
 * map.overlay.update
 * http://cmapi.org/versions/v1.3.0/docs/index.html#map.overlay.update
 *
 * Update the name an existing overlay or move the overlay to another parent overlay.
 *
 * Payload:
 *  {
 *      name: string (optional),
 *      overlayId: string (optional),
 *      parentId: string (optional)
 *  }
 */
public class MapOverlayUpdate {

    private String name;
    private String overlayId;
    private String parentId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOverlayId() {
        return overlayId;
    }

    public void setOverlayId(String overlayId) {
        this.overlayId = overlayId;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }
}
