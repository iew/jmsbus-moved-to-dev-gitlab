package org.cbrnwatchboard.jms.cmapi;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.google.gson.*;

/**
 * Unit test for MapFeaturePlot.
 */
public class MapFeatureTest
    extends MapTestCase
{

    private Gson gson = new Gson();
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public MapFeatureTest(String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( MapFeatureTest.class );
    }

    /**
     * Test
     */
    public void testPlot1()
    {
        String text = readFileAsString("map.feature.plot.1.json");
        MapFeaturePlot obj = gson.fromJson(text, MapFeaturePlot.class);
        assertEquals( obj.getName(), "World Trade Center");
        assertEquals( obj.getFeatureId(), "example.mapWidget.1" );
    }

    /**
     * Test
     */
    public void testPlot2()
    {
        String text = readFileAsString("map.feature.plot.2.json");
        MapFeaturePlot obj = gson.fromJson(text, MapFeaturePlot.class);
        assertEquals( obj.getName(), "World Trade Center");
        assertEquals( obj.getOverlayId(), "2d882141-0d9e-59d4-20bb-58e6d0460699.1" );
    }

    /**
     * Test
     */
    public void testPlotUrl1()
    {
        String text = readFileAsString("map.feature.plot.url.1.json");
        MapFeaturePlotUrl obj = gson.fromJson(text, MapFeaturePlotUrl.class);
        assertEquals( obj.getName(), "Samples");
        assertEquals( obj.getOverlayId(), "2d882141-0d9e-59d4-20bb-58e6d0460699.1" );
        assertEquals( obj.getUrl(), "https://developers.google.com/kml/documentation/KML_Samples.kml");
    }

    /**
     * Test
     */
    public void testPlotUrl2()
    {
        String text = readFileAsString("map.feature.plot.url.2.json");
        MapFeaturePlotUrl obj = gson.fromJson(text, MapFeaturePlotUrl.class);
        assertEquals( obj.getName(), "Bodies of Water");
        assertEquals( obj.getOverlayId(), "xyz" );
        assertEquals( obj.getUrl(), "http://demo.opengeo.org/geoserver/wms");
        assertEquals( obj.getParams().get("layers"), "topp:tasmania_water_bodies");
    }
}
