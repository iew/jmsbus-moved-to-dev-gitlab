package org.cbrnwatchboard.jms.cmapi;

import junit.framework.TestCase;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by gdonarum on 5/12/2016.
 */
public abstract class MapTestCase extends TestCase {
        /**
         * Create the test case
         *
         * @param testName name of the test case
         */
        public MapTestCase(String testName )
        {
            super( testName );
        }

    public String readFileAsString(String filePath) {
        try {
            StringBuffer fileData = new StringBuffer(1000);
            BufferedReader reader = new BufferedReader(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream(filePath)));
            char[] buf = new char[1024];
            int numRead = 0;
            while ((numRead = reader.read(buf)) != -1) {
                String readData = String.valueOf(buf, 0, numRead);
                fileData.append(readData);
                buf = new char[1024];
            }
            reader.close();
            return fileData.toString();
        } catch(Exception e) {

        }
        return "ERROR";
    }
}
